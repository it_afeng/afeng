package com.afeng.cache.config;

import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.afeng.util.log.DltagConstant;
import com.afeng.util.log.LogMessageUtil;

/**
 * 
 * @Title       : Ehcache3EventLogger
 * @Description : Ehcache3事件记录日志
 * @Date        : 2017年11月17日
 * @author      : afeng
 */
public class Ehcache3EventLogger implements CacheEventListener<Object, Object> {

    private static final Logger LOGGER = LoggerFactory.getLogger(Ehcache3EventLogger.class);

    @Override
    public void onEvent(CacheEvent<? extends Object, ? extends Object> event) {
        
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_EHCACHE3_EVENT_DLTAG);
        logMessageUtil.add("event", event.getType());
        logMessageUtil.add("key", event.getKey());
        logMessageUtil.add("oldValue", event.getOldValue());
        logMessageUtil.add("newValue", event.getNewValue());
        
        LOGGER.info(logMessageUtil.toString());

    }

}