package com.afeng.cache.config;
import java.util.concurrent.TimeUnit;

import javax.cache.CacheManager;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.Duration;
import javax.cache.expiry.TouchedExpiryPolicy;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.stereotype.Component;

/**
 * 
 * @Title       : Ehcache3Config
 * @Description : Ehcache3配置类
 * @Date        : 2017年11月17日
 * @author      : afeng
 */
@Component
public class Ehcache3Config implements JCacheManagerCustomizer {

    /**
     * 测试模块缓存名称定义
     */
    public static final String CACHE_NAME_PERSON = "person";
    public static final String CACHE_NAME_USER = "user";
    
    @Override
    public void customize(CacheManager cacheManager) {
        
        /*
         * 测试模块缓存 60s
         */
        cacheManager.createCache(CACHE_NAME_PERSON,
                new MutableConfiguration<>()
                        .setExpiryPolicyFactory(TouchedExpiryPolicy.factoryOf(new Duration(TimeUnit.SECONDS, 60)))
                        .setStoreByValue(true).setStatisticsEnabled(true));
        /*
         * 测试模块缓存 60s
         */
        cacheManager.createCache(CACHE_NAME_USER,
                new MutableConfiguration<>()
                        .setExpiryPolicyFactory(TouchedExpiryPolicy.factoryOf(new Duration(TimeUnit.SECONDS, 60)))
                        .setStoreByValue(true).setStatisticsEnabled(true));
    }

}
