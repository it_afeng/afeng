# 使用ehcache3实现缓存机制
	1、缓存的对象必须序列化。
	2、缓存对象最好重写toString方法，方便查看缓存事件log。
	3、ehcache缓存机制，比较适用单机服务缓存，也支持分布式，不过分布式最好用redis或者memcache。
	