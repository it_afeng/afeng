package com.afeng.memcached;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 
 * @Title       : MemcachedConfiguration
 * @Description : memcached配置类，指定自定义memcached配置文件
 * @Date        : 2017年11月24日
 * @author      : afeng
 */
@Component
@ConfigurationProperties(prefix = "memcached")
@PropertySource("classpath:memcached.properties")
public class MemcachedConfiguration {

    /**
     * memcache服务器集合，host:port 
     */
    private String[] servers;

    /**
     * 服务器的权重集合
     */
    private int[] weights;
    
    /**
     * 连接池大小，推荐在1-30之间为好，太大则浪费系统资源，太小无法达到分担负载的目的
     */
    private int connectionPoolSize = 5;

    public String[] getServers() {
        return servers;
    }

    public void setServers(String[] servers) {
        this.servers = servers;
    }

    public int[] getWeights() {
        return weights;
    }

    public void setWeights(int[] weights) {
        this.weights = weights;
    }

    public int getConnectionPoolSize() {
        return connectionPoolSize;
    }

    public void setConnectionPoolSize(int connectionPoolSize) {
        this.connectionPoolSize = connectionPoolSize;
    }

}