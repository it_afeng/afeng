package com.afeng.executor.demo;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class JobTest {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args)  {
		
	    CountDownLatch latch = new CountDownLatch(10);
	    System.out.println("主线程开始执行:"+System.currentTimeMillis());
		for(int i = 0;i<100;i++){
			JobTaskVo vo = new JobTaskVo("name_"+i, i);
			System.out.println(vo.getName());
			ThreadPoolExecutorManager.addTask(vo,latch);
//			try {
//				Thread.sleep(100);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
		}
		try {
		    //加上时间参数，设置主线程最大等待时间
            //latch.await(6000l,TimeUnit.MILLISECONDS);
		    latch.await();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		System.out.println("主线程执行完毕："+ System.currentTimeMillis());
		
		//关闭线程池
		ThreadPoolExecutorManager.close();
		
	}

}
