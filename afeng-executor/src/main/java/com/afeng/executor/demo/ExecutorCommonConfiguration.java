package com.afeng.executor.demo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 
 * @Title       : ExecutorCommonConfiguration
 * @Description : memcached配置类，指定自定义memcached配置文件
 * @Date        : 2017年12月26日
 * @author      : afeng
 */
@Component
@ConfigurationProperties(prefix = "executor.common")
@PropertySource("classpath:executor.properties")
public class ExecutorCommonConfiguration {

    /**
     * 线程池中保持的线程数，即使有些线程已经处于空闲状态，仍然保持存活
     */
    private int corePoolSize = 10;
    
    /**
     * 线程池最大值，最大边界是CAPACITY
     */
    private int maximumPoolSize = 100;
    
    /**
     * 空闲线程的等待时间
     */
    private int keepAliveTime = 1;
    
    /**
     * 队列大小
     */
    private int queueSize = 1000;

    public int getCorePoolSize() {
        return corePoolSize;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public void setMaximumPoolSize(int maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize;
    }

    public int getKeepAliveTime() {
        return keepAliveTime;
    }

    public void setKeepAliveTime(int keepAliveTime) {
        this.keepAliveTime = keepAliveTime;
    }

    public int getQueueSize() {
        return queueSize;
    }

    public void setQueueSize(int queueSize) {
        this.queueSize = queueSize;
    }

}