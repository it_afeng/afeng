package com.afeng.executor.demo;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import com.alibaba.fastjson.JSON;

/**
 * @Description : 执行任务的线程
 * @author      : yfbian
 */
public class JobWorkerThread implements Runnable {
	
	private JobTaskVo jobTaskVo = null;
	CountDownLatch latch;

	public JobWorkerThread(JobTaskVo jobTaskVo,CountDownLatch latch) {
		this.jobTaskVo = jobTaskVo;
		this.latch = latch;
	}
	
	@Override
	public void run() {
		
		String name = jobTaskVo.getName();
		Integer age = jobTaskVo.getAge();
		
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("name", name.toString());
		paramMap.put("age", age.toString());
		
		String json = JSON.toJSONString(paramMap);
		
		//logger.info(json);
		try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		System.out.println(Thread.currentThread().getName() + ",over!");
		latch.countDown();
		/*
		 * 任务完成后，计数器：-1
		 */
		ThreadPoolExecutorManager.sendWorkerSize.decrementAndGet();
		
	}
	
}
