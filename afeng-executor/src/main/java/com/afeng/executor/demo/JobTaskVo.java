package com.afeng.executor.demo;

/**
 * 
 * @Description : 任务vo
 * @Date        : 2017年12月28日
 * @author      : afeng
 */
public class JobTaskVo {

	public JobTaskVo() {

	}

	public JobTaskVo(String name,Integer age) {
		this.name = name;
		this.age = age;
	}

	private String name;
	private Integer age;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
	
}
