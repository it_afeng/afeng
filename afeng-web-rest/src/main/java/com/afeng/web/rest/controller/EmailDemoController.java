package com.afeng.web.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afeng.util.result.JsonResult;
import com.afeng.web.rest.service.EmailDemoService;

/**
 * 
 * @Description : 发送邮件示例控制器
 * @Date        : 2017年12月01日
 * @author      : afeng
 */
@RestController
@RequestMapping("emailDemo")
public class EmailDemoController {
    
    @Autowired
    public EmailDemoService emailDemoService;
    
    
    /**
     * 发送邮件简单测试
     * eg：http://localhost:8080/emailDemo/testEmail
     */
    @RequestMapping(value = "/testEmail")
    public Object testEmail() throws Exception {
        emailDemoService.testEmail();
        return JsonResult.success().setData("ok");
    }
    
}

