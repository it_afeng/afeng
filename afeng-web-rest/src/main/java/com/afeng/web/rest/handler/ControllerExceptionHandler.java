package com.afeng.web.rest.handler;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.afeng.util.log.DltagConstant;
import com.afeng.util.log.LogMessageUtil;
import com.afeng.util.result.JsonResult;

/**
 * 
 * @Title       : ControllerExceptionHandler
 * @Description : 注解Controlleradvice+ExceptionHandler实现全局的异常处理
 * @ExceptionHandler                注解说明：（缺点）进行异常处理的方法必须与出错的方法在同一个Controller里面
 * @ControllerAdvice                注解说明：完善@ExceptionHandler，不需要在同一个Controller中，实现全局处理的效果。
 * @Date        : 2017年11月27日
 * @author      : afeng
 */
@ControllerAdvice
public class ControllerExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler
    @ResponseBody
    public Object exceptionHandler(HttpServletRequest req, Exception ex) {

        StringBuffer errMsg = new StringBuffer();
        errMsg.append(ex.getMessage());
        for (StackTraceElement element : ex.getStackTrace()) {
            errMsg.append("$$");
            errMsg.append(element.toString());
        }

        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_ERROR);
        logMessageUtil.add("errType", ex.toString());
        logMessageUtil.add("errMsg", errMsg.toString());

        LOGGER.error(logMessageUtil.toString());

        JsonResult jsonResult = JsonResult.fail(ex.toString());// 定义返回对象
        return jsonResult;
    }
}
