package com.afeng.web.rest.service;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afeng.db.mysql.dao.DemoDao;
import com.afeng.domain.mysql.person.PersonDomain;
import com.afeng.util.log.DltagConstant;
import com.afeng.util.log.LogMessageUtil;
import com.afeng.web.rest.threadlocal.ParamsThreadlocal;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * 
 * @Title       : MysqlDemoService
 * @Description : Mysql增删改查示例服务类
 * @Date        : 2017年11月18日
 * @author      : afeng
 */
@Service
public class MysqlDemoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MysqlDemoService.class);

    @Autowired
    private DemoDao<PersonDomain> demoDao;
    
    /**
     * add demo
     */
    public Long add() throws Exception {
        
        String name = ParamsThreadlocal.PARAMS.get().get("name");
        String age = ParamsThreadlocal.PARAMS.get().get("age");
        String address = ParamsThreadlocal.PARAMS.get().get("address");
        
        PersonDomain personDomain = new PersonDomain();
        personDomain.setAge(Integer.parseInt(age));
        personDomain.setName(name);
        personDomain.setAddress(address);
        
        long startTime = System.currentTimeMillis();
        demoDao.insert(personDomain);
        long proc_time = System.currentTimeMillis() - startTime;
        
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_MYSQL_ADD_DLTAG);
        logMessageUtil.add("proc_time", proc_time);
        logMessageUtil.add("args", JSON.toJSON(personDomain));
        LOGGER.info(logMessageUtil.toString());
        
        return personDomain.getId();
    }
    
    /**
     * update demo
     */
    public Long update() throws Exception {
        
        String name = ParamsThreadlocal.PARAMS.get().get("name");
        String age = ParamsThreadlocal.PARAMS.get().get("age");
        String address = ParamsThreadlocal.PARAMS.get().get("address");
        String id = ParamsThreadlocal.PARAMS.get().get("id");
        
        PersonDomain personDomain = new PersonDomain();
        personDomain.setAge(Integer.parseInt(age));
        personDomain.setName(name);
        personDomain.setAddress(address);
        personDomain.setId(Long.parseLong(id));
        
        long startTime = System.currentTimeMillis();
        long updateCount = demoDao.update(personDomain);
        long proc_time = System.currentTimeMillis() - startTime;
        
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_MYSQL_UPDATE_DLTAG);
        logMessageUtil.add("proc_time", proc_time);
        logMessageUtil.add("args", JSON.toJSON(personDomain));
        LOGGER.info(logMessageUtil.toString());
        
        return updateCount;
    }
    
    /**
     * delete demo
     */
    public Long deleteById() throws Exception {
        
        String id = ParamsThreadlocal.PARAMS.get().get("id");
        
        long startTime = System.currentTimeMillis();
        long deleteCount = demoDao.deleteById(id);
        long proc_time = System.currentTimeMillis() - startTime;
        
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_MYSQL_DELETE_DLTAG);
        logMessageUtil.add("proc_time", proc_time);
        logMessageUtil.add("id", id);
        LOGGER.info(logMessageUtil.toString());
        
        return deleteCount;
    }
    
    /**
     * batch delete demo 
     */
    public Long deleteByIds() throws Exception {
        
        String ids = ParamsThreadlocal.PARAMS.get().get("ids");
        
        List<String> idList = Arrays.asList(ids.split(","));
        
        long startTime = System.currentTimeMillis();
        long deleteCount = demoDao.deleteByIds(idList);
        long proc_time = System.currentTimeMillis() - startTime;
        
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_MYSQL_DELETE_DLTAG);
        logMessageUtil.add("proc_time", proc_time);
        logMessageUtil.add("ids", ids);
        LOGGER.info(logMessageUtil.toString());
        
        return deleteCount;
    }
    
    /**
     * query demo
     */
    public PersonDomain queryById() throws Exception {
        
        String id = ParamsThreadlocal.PARAMS.get().get("id");
        
        long startTime = System.currentTimeMillis();
        PersonDomain personDomain = demoDao.queryById(id);
        long proc_time = System.currentTimeMillis() - startTime;
        
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_MYSQL_QUERY_DLTAG);
        logMessageUtil.add("proc_time", proc_time);
        logMessageUtil.add("id", id);
        LOGGER.info(logMessageUtil.toString());
        
        return personDomain;
        
    }
    
    /**
     * query all demo
     */
    public List<PersonDomain> queryAll() throws Exception {
        
        long startTime = System.currentTimeMillis();
        List<PersonDomain> personDomainList = demoDao.queryAll();
        long proc_time = System.currentTimeMillis() - startTime;
        
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_MYSQL_QUERY_DLTAG);
        logMessageUtil.add("proc_time", proc_time);
        LOGGER.info(logMessageUtil.toString());
        
        return personDomainList;
        
    }
    
    /**
     * query by condition demo
     */
    public List<PersonDomain> queryByCondition() throws Exception {
        
        String name = ParamsThreadlocal.PARAMS.get().get("name");
        String age = ParamsThreadlocal.PARAMS.get().get("age");
        String address = ParamsThreadlocal.PARAMS.get().get("address");
        
        PersonDomain personDomainCondition = new PersonDomain();
        if(StringUtils.isNotBlank(age)) {
            personDomainCondition.setAge(Integer.parseInt(age));
        }
        personDomainCondition.setName(name);
        personDomainCondition.setAddress(address);
        
        long startTime = System.currentTimeMillis();
        List<PersonDomain> personDomainList = demoDao.queryByCondition(personDomainCondition);
        long proc_time = System.currentTimeMillis() - startTime;
        
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_MYSQL_QUERY_DLTAG);
        logMessageUtil.add("proc_time", proc_time);
        logMessageUtil.add("args", JSON.toJSON(personDomainCondition));
        LOGGER.info(logMessageUtil.toString());
        
        return personDomainList;
    }
    
    
    
    /**
     * count query demo
     */
    public Long countAll() throws Exception {
        
        long startTime = System.currentTimeMillis();
        Long count = demoDao.countAll();
        long proc_time = System.currentTimeMillis() - startTime;
        
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_MYSQL_QUERY_DLTAG);
        logMessageUtil.add("proc_time", proc_time);
        logMessageUtil.add("count", count);
        LOGGER.info(logMessageUtil.toString());
        
        return count;
        
    }
    
    /**
     * count query by condition demo
     */
    public Long countByCondition() throws Exception {
        
        String name = ParamsThreadlocal.PARAMS.get().get("name");
        String age = ParamsThreadlocal.PARAMS.get().get("age");
        String address = ParamsThreadlocal.PARAMS.get().get("address");
        
        PersonDomain personDomainCondition = new PersonDomain();
        if(StringUtils.isNotBlank(age)) {
            personDomainCondition.setAge(Integer.parseInt(age));
        }
        personDomainCondition.setName(name);
        personDomainCondition.setAddress(address);
        
        long startTime = System.currentTimeMillis();
        Long count = demoDao.countByCondition(personDomainCondition);
        long proc_time = System.currentTimeMillis() - startTime;
        
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_MYSQL_QUERY_DLTAG);
        logMessageUtil.add("proc_time", proc_time);
        logMessageUtil.add("args", JSON.toJSON(personDomainCondition));
        logMessageUtil.add("count", count);
        LOGGER.info(logMessageUtil.toString());
        
        return count;
    }
    
    /**
     * query all page demo
     */
    public Page<PersonDomain> queryAllPage(int pageNo, int pageSize) throws Exception {
        
        long startTime = System.currentTimeMillis();
        PageHelper.startPage(pageNo, pageSize);//开始分页
        Page<PersonDomain> personDomainPage = demoDao.queryAllPage();
        PageHelper.clearPage();//移除分页本地变量
        long proc_time = System.currentTimeMillis() - startTime;
        
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_MYSQL_QUERY_DLTAG);
        logMessageUtil.add("pageNo", pageNo);
        logMessageUtil.add("pageSize", pageSize);
        logMessageUtil.add("proc_time", proc_time);
        LOGGER.info(logMessageUtil.toString());
        
        return personDomainPage;
        
    }
    
    /**
     * query by condition page demo
     */
    public Page<PersonDomain> queryByConditionPage(int pageNo, int pageSize) throws Exception {
        
        String name = ParamsThreadlocal.PARAMS.get().get("name");
        String age = ParamsThreadlocal.PARAMS.get().get("age");
        String address = ParamsThreadlocal.PARAMS.get().get("address");
        
        PersonDomain personDomainCondition = new PersonDomain();
        if(StringUtils.isNotBlank(age)) {
            personDomainCondition.setAge(Integer.parseInt(age));
        }
        personDomainCondition.setName(name);
        personDomainCondition.setAddress(address);
        
        long startTime = System.currentTimeMillis();
        PageHelper.startPage(pageNo, pageSize);//开始分页
        Page<PersonDomain> personDomainPage = demoDao.queryByConditionPage(personDomainCondition);
        PageHelper.clearPage();//移除分页本地变量
        long proc_time = System.currentTimeMillis() - startTime;
        
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_MYSQL_QUERY_DLTAG);
        logMessageUtil.add("proc_time", proc_time);
        logMessageUtil.add("pageNo", pageNo);
        logMessageUtil.add("pageSize", pageSize);
        logMessageUtil.add("args", JSON.toJSON(personDomainCondition));
        LOGGER.info(logMessageUtil.toString());
        
        return personDomainPage;
    }

}
