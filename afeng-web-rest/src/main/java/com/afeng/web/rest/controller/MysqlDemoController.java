package com.afeng.web.rest.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afeng.domain.mysql.person.PersonDomain;
import com.afeng.util.result.JsonResult;
import com.afeng.web.rest.service.MysqlDemoService;
import com.github.pagehelper.Page;

/**
 * 
 * @Title       : MysqlDemoController
 * @Description : Mysql示例控制器
 * @Date        : 2017年11月14日
 * @author      : afeng
 */
@RestController
@RequestMapping("mysqlDemo")
public class MysqlDemoController {
    
    @Autowired
    public MysqlDemoService mysqlDemoService;
    
    /**
     * 新增记录
     * eg：http://localhost:8080/mysqlDemo/add?name=张无忌&age=27&address=东风路100号
     */
    @RequestMapping(value = "/add")
    public Object add() throws Exception {
        return JsonResult.success().setData(mysqlDemoService.add());
    }
    
    /**
     * 修改记录
     * eg：http://localhost:8080/mysqlDemo/update?name=张无忌&age=27&address=东风路110号&id=3
     */
    @RequestMapping(value = "/update")
    public Object update() throws Exception {
        return JsonResult.success().setData(mysqlDemoService.update());
    }
    
    /**
     * 单条删除记录（根据id）
     * eg：http://localhost:8080/mysqlDemo/deleteById?id=3
     */
    @RequestMapping(value = "/deleteById")
    public Object deleteById() throws Exception {
        return JsonResult.success().setData(mysqlDemoService.deleteById());
    }
    
    /**
     * 批量删除记录（根据ids）
     * eg：http://localhost:8080/mysqlDemo/deleteByIds?ids=3,5,6
     */
    @RequestMapping(value = "/deleteByIds")
    public Object deleteByIds() throws Exception {
        return JsonResult.success().setData(mysqlDemoService.deleteByIds());
    }
    
    /**
     * 根据id查看记录
     * eg：http://localhost:8080/mysqlDemo/queryById?id=9
     */
    @RequestMapping(value = "/queryById")
    public Object queryById() throws Exception {
        return JsonResult.success().setData(mysqlDemoService.queryById());
    }
    
    /**
     * 查看所有记录
     * eg：http://localhost:8080/mysqlDemo/queryAll
     */
    @RequestMapping(value = "/queryAll")
    public Object queryAll() throws Exception {
        return JsonResult.success().setData(mysqlDemoService.queryAll()).setTotal(mysqlDemoService.countAll());
    }
    
    /**
     * 查看所有记录page
     * eg：http://localhost:8080/mysqlDemo/queryAllPage?pageNo=1&pageSize=5
     */
    @RequestMapping(value = "/queryAllPage")
    public Object queryAllPage(int pageNo, int pageSize) throws Exception {
        Page<PersonDomain> personDomainPage = mysqlDemoService.queryAllPage(pageNo,pageSize);
        return JsonResult.success().setData(personDomainPage.toPageInfo());
    }
    
    /**
     * 查看符合条件的记录
     * eg：http://localhost:8080/mysqlDemo/queryByCondition?age=30
     */
    @RequestMapping(value = "/queryByCondition")
    public Object queryByCondition(HttpServletRequest request) throws Exception {
        return JsonResult.success().setData(mysqlDemoService.queryByCondition()).
                setTotal(mysqlDemoService.countByCondition());
    }
    
    /**
     * 查看符合条件的记录
     * eg：http://localhost:8080/mysqlDemo/queryByConditionPage?age=30&pageNo=1&pageSize=5
     */
    @RequestMapping(value = "/queryByConditionPage")
    public Object queryByConditionPage(int pageNo, int pageSize) throws Exception {
        Page<PersonDomain> personDomainPage = mysqlDemoService.queryByConditionPage(pageNo,pageSize);
        return JsonResult.success().setData(personDomainPage.toPageInfo());
    }
    
    /**
     * 查看所有记录count
     * eg：http://localhost:8080/mysqlDemo/countAll
     */
    @RequestMapping(value = "/countAll")
    public Object countAll(HttpServletRequest request) throws Exception {
        return JsonResult.success().setData(mysqlDemoService.countAll());
    }
    
    /**
     * 查看符合条件的记录
     * eg：http://localhost:8080/mysqlDemo/countByCondition?age=30
     */
    @RequestMapping(value = "/countByCondition")
    public Object countByCondition(HttpServletRequest request) throws Exception {
        return JsonResult.success().setData(mysqlDemoService.countByCondition());
    }
    
}

