//package com.afeng.web.rest.controller;
//
//import org.springframework.beans.factory.annotation.Autowired;
//
//import org.springframework.web.bind.annotation.*;
//
//import com.afeng.es.domain.City;
//import com.afeng.web.rest.service.EsDemoService;
//
//import java.util.List;
//
///**
// * 
// * @Description : es示例控制器
// * @Date : 2017年11月28日
// * @author : afeng
// */
//@RestController
//@RequestMapping("esDemo")
//public class EsDemoController {
//
//    @Autowired
//    private EsDemoService esDemoService;
//
//    /**
//     * 新增
//     * eg：http://localhost:8080/esDemo/save?id=2&name=河南&description=古都&score=80
//     */
//    @RequestMapping(value = "/save")
//    public Long save(@RequestParam(value = "id") Long id, @RequestParam(value = "name") String name,
//            @RequestParam(value = "description") String description, @RequestParam(value = "score") Integer score) {
//        City city = new City();
//        city.setId(id);
//        city.setName(name);
//        city.setDescription(description);
//        city.setScore(score);
//        return esDemoService.saveCity(city);
//    }
//
//    /**
//     * 查询单条
//     * eg：http://localhost:8080/esDemo/findOneById?id=2
//     */
//    @RequestMapping(value = "/findOneById")
//    public City save(@RequestParam(value = "id") Long id) {
//        return esDemoService.findOneById(id);
//    }
//    
//    /**
//     * 查询多条
//     * eg：http://localhost:8080/esDemo/findByDescription?description=古都
//     */
//    @RequestMapping(value = "/findByDescription")
//    public List<City> findByDescription(@RequestParam(value = "description") String description) {
//        return esDemoService.findByDescription(description);
//    }
//    
//    /**
//     * not 查询
//     * eg：http://localhost:8080/esDemo/findByDescriptionNot?description=古都
//     */
//    @RequestMapping(value = "/findByDescriptionNot")
//    public List<City> findByDescriptionNot(@RequestParam(value = "description") String description) {
//        return esDemoService.findByDescriptionNot(description);
//    }
//    
//    /**
//     * and 查询
//     * eg：http://localhost:8080/esDemo/findByDescriptionAndScore?description=古都&score=80
//     */
//    @RequestMapping(value = "/findByDescriptionAndScore")
//    public List<City> findByDescriptionAndScore(@RequestParam(value = "description") String description,
//            @RequestParam(value = "score") Integer score) {
//        return esDemoService.findByDescriptionAndScore(description, score);
//    }
//
//    /**
//     * or 查询
//     * eg：http://localhost:8080/esDemo/findByDescriptionOrScore?description=古都&score=80
//     */
//    @RequestMapping(value = "/findByDescriptionOrScore")
//    public List<City> findByDescriptionOrScore(@RequestParam(value = "description") String description,
//            @RequestParam(value = "score") Integer score) {
//        return esDemoService.findByDescriptionOrScore(description, score);
//    }
//
//    /**
//     * like 查询
//     * eg：http://localhost:8080/esDemo/findByDescriptionLike?description=古
//     */
//    @RequestMapping(value = "/findByDescriptionLike")
//    public List<City> findByDescriptionLike(@RequestParam(value = "description") String description) {
//        return esDemoService.findByDescriptionLike(description);
//    }
//}