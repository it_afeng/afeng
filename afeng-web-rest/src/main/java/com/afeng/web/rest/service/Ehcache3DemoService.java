package com.afeng.web.rest.service;

import org.apache.ibatis.cache.CacheException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.afeng.cache.config.Ehcache3Config;
import com.afeng.db.mysql.dao.DemoDao;
import com.afeng.domain.mysql.person.PersonDomain;
import com.afeng.util.log.DltagConstant;
import com.afeng.util.log.LogMessageUtil;
import com.afeng.web.rest.threadlocal.ParamsThreadlocal;

/**
 * 
 * @Title       : Ehcache3DemoService
 * @Description : Ehcache3示例服务类
 * @Date        : 2017年11月20日
 * @author      : afeng
 */
@Service
public class Ehcache3DemoService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(Ehcache3DemoService.class);

    //这里的单引号不能少，否则会报错，被识别是一个对象
    private static final String CACHE_KEY = "'person_'";

    @Autowired
    private DemoDao<PersonDomain> demoDao;

    /**
     * 根据id查找用户数据
     */
    @Cacheable(value=Ehcache3Config.CACHE_NAME_PERSON,key= CACHE_KEY + "+#id")
    public PersonDomain queryById(Long id){
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.add("msg", "没有走ehcache。");
        
        long startTime = System.currentTimeMillis();
        PersonDomain personDomain = demoDao.queryById(id.toString());
        long proc_time = System.currentTimeMillis() - startTime;
        
        logMessageUtil.setDltag(DltagConstant.COM_MYSQL_QUERY_DLTAG);
        logMessageUtil.add("proc_time", proc_time);
        logMessageUtil.add("id", id);
        LOGGER.info(logMessageUtil.toString());
        
        
        return personDomain;
    }
    
    /**
     * 更新用户数据
     */
    @CachePut(value = Ehcache3Config.CACHE_NAME_PERSON,key = CACHE_KEY + "+#id")
    public PersonDomain update(Long id) throws CacheException{
        
        PersonDomain personDomainCondition = new PersonDomain();
        String name = ParamsThreadlocal.PARAMS.get().get("name");
        String age = ParamsThreadlocal.PARAMS.get().get("age");
        personDomainCondition.setId(id);
        personDomainCondition.setName(name);
        personDomainCondition.setAge(Integer.parseInt(age));
        
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        
        long startTime = System.currentTimeMillis();
        demoDao.update(personDomainCondition);
        long proc_time = System.currentTimeMillis() - startTime;
        
        logMessageUtil.setDltag(DltagConstant.COM_MYSQL_UPDATE_DLTAG);
        logMessageUtil.add("proc_time", proc_time);
        LOGGER.info(logMessageUtil.toString());
        
        return personDomainCondition;
    }
    
    /**
     * 删除用户数据(清除缓存)
     */
    @CacheEvict(value = Ehcache3Config.CACHE_NAME_PERSON,key = CACHE_KEY + "+#id")
    public void deleteById(Long id){
        
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        
        long startTime = System.currentTimeMillis();
        demoDao.deleteById(id.toString());
        long proc_time = System.currentTimeMillis() - startTime;
        
        logMessageUtil.setDltag(DltagConstant.COM_MYSQL_DELETE_DLTAG);
        logMessageUtil.add("proc_time", proc_time);
        LOGGER.info(logMessageUtil.toString());
        
    }

}
