//package com.afeng.web.rest.service;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.afeng.domain.mongodb.user.Users;
//import com.afeng.mongodb.dao.MongoBaseDao;
//import com.afeng.util.log.DltagConstant;
//import com.afeng.util.log.LogMessageUtil;
//import com.alibaba.fastjson.JSON;
//
///**
// * 
// * @Title       : MongoDemoService
// * @Description : mongo 示例服务类
// * @Date        : 2017年11月26日
// * @author      : afeng
// */
//@Service
//public class MongoDemoService {
//    
//    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDemoService.class);
//
//    /**
//     * mongodb客户度工具类
//     */
//    @Autowired
//    MongoBaseDao<Users> mongoBaseDao;
//
//    /**
//     * 测试mongo的增、查
//     */
//    public Object testMongo() throws Exception{
//        
//        /**
//         * test save
//         */
//        Users user = new Users();
//        user.setUserid("999999");
//        user.setPassword("123456");
//        mongoBaseDao.save(user);
//        
//        /*
//         * 记录业务日志
//         */
//        LogMessageUtil logMessageUtil = new LogMessageUtil();
//        logMessageUtil.setDltag(DltagConstant.COM_MONGO_SAVE_DLTAG);
//        logMessageUtil.add("agrs", JSON.toJSONString(user));
//        LOGGER.info(logMessageUtil.toString());
//        
//        /*特别注意：字段全部小写，否则无法定位到记录。
//         * 
//         */
//        mongoBaseDao.deleteById("userid", "999999", Users.class);
//        
//        return mongoBaseDao.findById("userid", "afeng", Users.class);
//    }
//    
//}
