package com.afeng.web.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.afeng.web.rest.interceptor.LoggerInterceptor;
import com.afeng.web.rest.interceptor.ParamsInterceptor;

/**
 * 
 * @Title       : InterceptorConfiguration
 * @Description : 拦截器配置类
 * @Date        : 2017年11月14日
 * @author      : afeng
 */
@Configuration
public class InterceptorConfiguration extends WebMvcConfigurerAdapter {

    private static final String ALL_PATH = "/**";

    /**
     * 日志拦截器（注意，需要第一个注册）
     */
    @Autowired
    private LoggerInterceptor loggerInterceptor;
    
    /**
     * 参数拦截器
     */
    @Autowired
    private ParamsInterceptor paramsInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //日志拦截器（注意，需要第一个注册）
        registry.addInterceptor(loggerInterceptor).addPathPatterns(ALL_PATH);
        //参数拦截器
        registry.addInterceptor(paramsInterceptor).addPathPatterns(ALL_PATH);
    }

}