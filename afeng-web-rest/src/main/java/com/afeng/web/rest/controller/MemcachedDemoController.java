//package com.afeng.web.rest.controller;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.afeng.web.rest.service.MemcachedDemoService;
//
///**
// * 
// * @Title       : MemcachedDemoController
// * @Description : Memcached缓存示例控制器
// * @Date        : 2017年11月24日
// * @author      : afeng
// */
//@RestController
//@RequestMapping(value = "/memcachedDemo")
//public class MemcachedDemoController {
//    
//    @Autowired
//    MemcachedDemoService memcachedDemoService;
//
//    @RequestMapping("/testMemcached")
//    public Object cacheSave(@RequestParam(value = "id", required = false, defaultValue = "228") int id) throws Exception  {
//
//        return memcachedDemoService.testMemcached();
//    }
//}