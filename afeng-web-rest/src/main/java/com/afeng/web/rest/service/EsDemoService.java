//package com.afeng.web.rest.service;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
//import org.springframework.stereotype.Service;
//
//import com.afeng.es.domain.City;
//import com.afeng.es.repository.demo.CityRepository;
//
//import java.util.List;
//
///**
// * 
// * @Description : 城市es服务逻辑类
// * @Date        : 2017年11月28日
// * @author      : afeng
// */
//@Service
//public class EsDemoService {
//
//    private static final Logger LOGGER = LoggerFactory.getLogger(EsDemoService.class);
//
//    // 分页参数 -> TODO 代码可迁移到具体项目的公共 common 模块
//    private static final Integer pageNumber = 0;
//    private static final Integer pageSize = 10;
//    Pageable pageable = new PageRequest(pageNumber, pageSize);
//
//    @Autowired
//    CityRepository cityRepository;
//
//    /**
//     * 插入
//     */
//    public Long saveCity(City city) {
//        City cityResult = cityRepository.save(city);
//        return cityResult.getId();
//    }
//    
//    /**
//     * 查询单条
//     */
//    public City findOneById(Long id) {
//        City cityResult = cityRepository.findOne(id);
//        return cityResult;
//    }
//    
//    /**
//     * 查询多条
//     */
//    public List<City> findByDescription(String description) {
//        return cityRepository.findByDescription(description, pageable).getContent();
//    }
//    
//    /**
//     * not 查询
//     */
//    public List<City> findByDescriptionNot(String description) {
//        return cityRepository.findByDescriptionNot(description, pageable).getContent();
//    }
//
//    /**
//     * and 查询
//     */
//    public List<City> findByDescriptionAndScore(String description, Integer score) {
//        return cityRepository.findByDescriptionAndScore(description, score);
//    }
//
//    /**
//     * or 查询
//     */
//    public List<City> findByDescriptionOrScore(String description, Integer score) {
//        return cityRepository.findByDescriptionOrScore(description, score);
//    }
//
//    /**
//     * like 查询
//     */
//    public List<City> findByDescriptionLike(String description) {
//        return cityRepository.findByDescriptionLike(description, pageable).getContent();
//    }
//
//}