package com.afeng.web.rest.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.afeng.util.config.KeyValueConfigurationUtil;
import com.afeng.util.log.DltagConstant;
import com.afeng.util.log.LogMessageUtil;
import com.afeng.web.rest.threadlocal.ParamsThreadlocal;

/**
 * 
 * @Title       : KeyValueConfigDemoService
 * @Description : KeyValue配置文件示例服务类
 * @Date        : 2017年11月17日
 * @author      : afeng
 */
@Service
public class KeyValueConfigDemoService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(KeyValueConfigDemoService.class);

    /**
     * 根据支付渠道id，获取支付渠道名称
     */
    public String getNameByChannelId() throws Exception {
        
        /*
         * 获取支付渠道id和name
         */
        String payChannelId = ParamsThreadlocal.PARAMS.get().get("payChannelId");
        String payChannelName = KeyValueConfigurationUtil.payChannelConfiguration.getNameById(payChannelId);
        
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_READ_CONFIG_DLTAG);
        logMessageUtil.add("payChannelId", payChannelId);
        logMessageUtil.add("payChannelName", payChannelName);
        LOGGER.info(logMessageUtil.toString());
        
        return payChannelName;
    }

}
