package com.afeng.web.rest.threadlocal;

import java.util.Map;

/**
 * 
 * @Title       : ParamsThreadlocal
 * @Description : 请求参数threadlocal定义类
 * @Date        : 2017年11月14日
 * @author      : afeng
 */
public class ParamsThreadlocal {

    /**
     * 请求参数threadlocal对象
     */
    public static ThreadLocal<Map<String, String>> PARAMS = new InheritableThreadLocal<>();

}
