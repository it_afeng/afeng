package com.afeng.web.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.afeng.util.spring.SpringContextUtil;

/**
 * 
 * @Title       : App
 * @Description : 项目启动入口类
 * @Date        : 2017年11月13日
 * @author      : afeng
 * @Configuration           注解说明：标记该类是一个配置文件，并初始化配置
 * @ComponentScan           注解说明：Spring自动扫描并载入bean容器
 * @EnableAutoConfiguration 注解说明：开启默认配置
 * @EnableAsync             注解说明：开启异步调用
 * @EnableCaching           注解说明：开启缓存功能（ehcache或redis都需要开启）
 * @EnableScheduling        注解说明：开启定时调度
 */
@Configuration
@ComponentScan(value = { "com.afeng" })
@EnableAutoConfiguration
@EnableAsync
@EnableCaching
@EnableScheduling
public class App {
    public static void main(String[] args) {
        ApplicationContext app = SpringApplication.run(App.class, args);
        //设置spring上下文对象
        SpringContextUtil.setApplicationContext(app);
    }
}
