//package com.afeng.web.rest.controller;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.afeng.web.rest.service.MongoDemoService;
//
///**
// * 
// * @Title       : MongoDemoController
// * @Description : mongo缓存示例控制器
// * @Date : 2017年11月26日
// * @author : afeng
// */
//@RestController
//@RequestMapping(value = "/mongoDemo")
//public class MongoDemoController {
//
//    @Autowired
//    MongoDemoService mongoDemoService;
//
//    @RequestMapping("/testMongo")
//    public Object testMongo() throws Exception {
//        return mongoDemoService.testMongo();
//    }
//}