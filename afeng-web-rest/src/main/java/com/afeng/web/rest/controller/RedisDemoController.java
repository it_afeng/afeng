package com.afeng.web.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afeng.util.result.JsonResult;
import com.afeng.web.rest.service.RedisDemoService;

/**
 * 
 * @Title       : RedisDemoController
 * @Description : redis缓存示例控制器
 * @Date        : 2017年11月20日
 * @author      : afeng
 */
@RestController
@RequestMapping("redisDemo")
public class RedisDemoController {
    
    @Autowired
    public RedisDemoService redisDemoService;
    
    
    /**
     * 根据id查看记录
     * eg：http://localhost:8080/redisDemo/testRedis
     */
    @RequestMapping(value = "/testRedis")
    public Object testRedis() throws Exception {
        String personDomain = redisDemoService.testRedis();
        return JsonResult.success().setData(personDomain);
    }
    
}

