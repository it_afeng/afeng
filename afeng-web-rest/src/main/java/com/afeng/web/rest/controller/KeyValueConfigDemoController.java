package com.afeng.web.rest.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afeng.util.result.JsonResult;
import com.afeng.web.rest.service.KeyValueConfigDemoService;

/**
 * 
 * @Title       : KeyValueConfigDemoController
 * @Description : KeyValue配置文件示例控制器
 * @Date        : 2017年11月17日
 * @author      : afeng
 */
@RestController
@RequestMapping("keyValueConfigDemo")
public class KeyValueConfigDemoController {
    
    /**
     * KeyValue配置文件示例服务类
     */
    @Autowired
    public KeyValueConfigDemoService keyValueConfigDemoService;
    
    /**
     * 根据支付渠道id，获取支付渠道名称
     * eg：http://localhost:8080/keyValueConfigDemo/getNameByChannelId?payChannelId=106
     * 具体key/value对应，查看配置文件：pay-channel.properties
     */
    @RequestMapping(value = "/getNameByChannelId")
    public Object getNameByChannelId(HttpServletRequest request) throws Exception {
        String resultData = keyValueConfigDemoService.getNameByChannelId();
        
        return JsonResult.success().setData(resultData);
    }

}