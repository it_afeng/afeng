package com.afeng.web.rest.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afeng.redis.config.RedisConfigUtil;
import com.afeng.util.log.DltagConstant;
import com.afeng.util.log.LogMessageUtil;

/**
 * 
 * @Title       : RedisDemoService
 * @Description : Redis 示例服务类
 * @Date        : 2017年11月21日
 * @author      : afeng
 */
@Service
public class RedisDemoService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(RedisDemoService.class);

    /**
     * redis模版操作工具类
     */
    @Autowired
    private RedisConfigUtil redisConfigUtil;

    /**
     * 测试redis的增、删、查（相对简单，一个方法带过）
     */
    public String testRedis(){
        
        /**
         * test set String
         */
        long startTime = System.currentTimeMillis();
        redisConfigUtil.set("key1","i am redis value ...");
        long proc_time = System.currentTimeMillis() - startTime;
        
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_REDIS_SET_DLTAG);
        logMessageUtil.add("proc_time", proc_time);
        logMessageUtil.add("key", "key1");
        logMessageUtil.add("value", "i am redis value ...");
        LOGGER.info(logMessageUtil.toString());
        
        /**
         * test get String
         */
        startTime = System.currentTimeMillis();
        String str = redisConfigUtil.get("key1");
        proc_time = System.currentTimeMillis() - startTime;
        
        /*
         * 记录业务日志
         */
        logMessageUtil.setDltag(DltagConstant.COM_REDIS_GET_DLTAG);
        logMessageUtil.add("proc_time", proc_time);
        logMessageUtil.add("key", "key1");
        logMessageUtil.add("value", str);
        LOGGER.info(logMessageUtil.toString());
        
        /**
         * test delete
         */
        startTime = System.currentTimeMillis();
        redisConfigUtil.delete("listObj");
        proc_time = System.currentTimeMillis() - startTime;
        /*
         * 记录业务日志
         */
        logMessageUtil.setDltag(DltagConstant.COM_REDIS_DELETE_DLTAG);
        logMessageUtil.add("proc_time", proc_time);
        logMessageUtil.add("key", "listObj");
        LOGGER.info(logMessageUtil.toString());
        
        return str;
    }
    
}
