package com.afeng.web.rest.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afeng.domain.mongodb.user.Users;
import com.afeng.email.config.MailConfig;
import com.afeng.util.log.DltagConstant;
import com.afeng.util.log.LogMessageUtil;
import com.alibaba.fastjson.JSON;

/**
 * 
 * @Description : 发送邮件示例服务类
 * @Date        : 2017年12月01日
 * @author      : afeng
 */
@Service
public class EmailDemoService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailDemoService.class);

    /**
     * 邮件工具类
     */
    @Autowired
    MailConfig mailConfig;

    /**
     * 测试邮件操作
     */
    public void testEmail() throws Exception{
        
        /**
         * test save
         */
        Users user = new Users();
        user.setUserid("999999");
        user.setPassword("123456");
        mailConfig.sendSimpleMail("416352221@qq.com", "测试", JSON.toJSONString(user));
        
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_SEND_EMAIL_DLTAG);
        logMessageUtil.add("agrs", JSON.toJSONString(user));
        LOGGER.info(logMessageUtil.toString());
        
        return ;
    }
    
}
