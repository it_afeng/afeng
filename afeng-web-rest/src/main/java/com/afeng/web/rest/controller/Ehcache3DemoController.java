package com.afeng.web.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afeng.domain.mysql.person.PersonDomain;
import com.afeng.util.result.JsonResult;
import com.afeng.web.rest.service.Ehcache3DemoService;
import com.afeng.web.rest.threadlocal.ParamsThreadlocal;

/**
 * 
 * @Title       : Ehcache3DemoController
 * @Description : Ehcache3缓存示例控制器
 * @Date        : 2017年11月20日
 * @author      : afeng
 */
@RestController
@RequestMapping("ehcache3Demo")
public class Ehcache3DemoController {
    
    @Autowired
    public Ehcache3DemoService ehcache3DemoService;
    
    
    /**
     * 根据id查看记录
     * eg：http://localhost:8080/ehcache3Demo/queryById?id=9
     */
    @RequestMapping(value = "/queryById")
    public Object queryById(long id) throws Exception {
        PersonDomain personDomain = ehcache3DemoService.queryById(id);
        return JsonResult.success().setData(personDomain);
    }
    
    /**
     * 更新记录
     * eg：http://localhost:8080/ehcache3Demo/update?id=9&age=40&name=唐三
     */
    @RequestMapping(value = "/update")
    public Object update() throws Exception {
        String id = ParamsThreadlocal.PARAMS.get().get("id");
        PersonDomain personDomain = ehcache3DemoService.update(Long.parseLong(id));
        return JsonResult.success().setData(personDomain);
    }
    
    /**
     * 删除记录
     * eg：http://localhost:8080/ehcache3Demo/deleteById?id=9
     */
    @RequestMapping(value = "/deleteById")
    public Object deleteById(long id) throws Exception {
        ehcache3DemoService.deleteById(id);
        return JsonResult.success();
    }

}

