//package com.afeng.web.rest.service;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.afeng.util.log.DltagConstant;
//import com.afeng.util.log.LogMessageUtil;
//
//import net.rubyeye.xmemcached.MemcachedClient;
//
///**
// * 
// * @Title       : MemcachedDemoService
// * @Description : Memcached 示例服务类
// * @Date        : 2017年11月24日
// * @author      : afeng
// */
//@Service
//public class MemcachedDemoService {
//    
//    private static final Logger LOGGER = LoggerFactory.getLogger(MemcachedDemoService.class);
//
//    /**
//     * memcached客户度工具类
//     */
//    @Autowired
//    MemcachedClient memCachedClient;
//
//    /**
//     * 测试memcached的增、查
//     */
//    public String testMemcached() throws Exception{
//        
//        String result = "";
//        
//        /**
//         * test set and get String
//         */
//            memCachedClient.set("id", 100,"1234567654321" );
//            memCachedClient.set("name", 100,"afeng111" );
//            memCachedClient.set("age", 100,"31" );
//            memCachedClient.set("sex", 100,"MALE" );
//            result = memCachedClient.get("id");
//        
//        /*
//         * 记录业务日志
//         */
//        LogMessageUtil logMessageUtil = new LogMessageUtil();
//        logMessageUtil.setDltag(DltagConstant.COM_MEMCACHED_SET_DLTAG);
//        logMessageUtil.add("id", "1234567654321");
//        logMessageUtil.add("name", "afeng111");
//        logMessageUtil.add("age", "31");
//        logMessageUtil.add("sex", "MALE");
//        LOGGER.info(logMessageUtil.toString());
//        
//        return result;
//    }
//    
//}
