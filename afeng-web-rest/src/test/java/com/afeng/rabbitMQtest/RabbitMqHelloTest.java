package com.afeng.rabbitMQtest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.afeng.rabbitMQ.sender.HelloSender;
import com.afeng.web.rest.App;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class RabbitMqHelloTest {

    @Autowired
    private HelloSender helloSender;

    @Test
    public void hello() throws Exception {
        //helloSender.send1();
        //helloSender.send2();
    }

}