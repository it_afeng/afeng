package com.afeng.domain.mysql.person;

import java.io.Serializable;

/**
 * 
 * @Title       : PersonDomain
 * @Description : demo模块域对象定义
 * @Date        : 2017年11月15日
 * @author      : afeng
 */
public class PersonDomain implements Serializable{
    
    private static final long serialVersionUID = 7646494611148175714L;
    
    /**
     * 主键id
     */
    private Long id;
    /**
     * 姓名
     */
    private String name;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 地址
     */
    private String address;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    @Override
    public String toString() {
        return "PersonDomain [id=" + id + ", name=" + name + ", age=" + age + ", address=" + address + "]";
    }

}
