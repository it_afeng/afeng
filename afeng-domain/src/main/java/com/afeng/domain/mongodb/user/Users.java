package com.afeng.domain.mongodb.user;

/**
 * 
 * @Title       : Users
 * @Description : demo模块域对象定义
 * @Date        : 2017年11月26日
 * @author      : afeng
 */
public class Users {

    /**
     * 用户id
     */
    private String userid;
    /**
     * 密码
     */
    private String password;
    
    public String getUserid() {
        return userid;
    }
    public void setUserid(String userid) {
        this.userid = userid;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    
}
