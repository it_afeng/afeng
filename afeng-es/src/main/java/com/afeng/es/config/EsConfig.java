package com.afeng.es.config;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import java.net.InetAddress;

/**
 * 
 * @Description : es配置类，指定自定义es配置文件
 * @Date        : 2017年11月28日
 * @author      : afeng
 */
@Configuration
@PropertySource("classpath:es.properties")
@EnableElasticsearchRepositories(basePackages = "com.afeng.es.repository.demo")
public class EsConfig {

    @Value("${elasticsearch.test.host}")
    private String esHost;

    @Value("${elasticsearch.test.port}")
    private int esPort;

    @Value("${elasticsearch.test.clustername}")
    private String esClusterName;

    @Bean
    public Client client() throws Exception {

        Settings esSettings = Settings.settingsBuilder()
                //.put("cluster.name", esClusterName)
                .build();

        return TransportClient.builder().settings(esSettings).build()
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(esHost), esPort));
    }

}