package com.afeng.es.repository.demo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.afeng.es.domain.City;

import java.util.List;

/**
 * 
 * @Description : 城市es操作类
 * @Date        : 2017年11月28日
 * @author      : afeng
 */
public interface CityRepository extends ElasticsearchRepository<City, Long> {
    /**
     * and 语句查询
     */
    List<City> findByDescriptionAndScore(String description, Integer score);

    /**
     * or 语句查询
     */
    List<City> findByDescriptionOrScore(String description, Integer score);

    /**
     * 根据条件查询多条
     */
    Page<City> findByDescription(String description, Pageable page);

    /**
     * not 语句查询
     */
    Page<City> findByDescriptionNot(String description, Pageable page);

    /**
     * like 语句查询
     */
    Page<City> findByDescriptionLike(String description, Pageable page);

}