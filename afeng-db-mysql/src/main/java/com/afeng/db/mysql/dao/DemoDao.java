package com.afeng.db.mysql.dao;

import java.util.List;

import com.github.pagehelper.Page;

/**
 * 
 * @Title       : DemoDao
 * @Description : demo模块dao数据查询接口(每个数据源需要配置一个独立的)
 * @Date        : 2017年11月15日
 * @author      : afeng
 */
public interface DemoDao<T> {
    
    /**
     * 单条记录新增
     */
    public Long insert(T domain);
    
    /**
     * 根据条件更新记录
     */
    public Long update(T domain);
    
    /**
     * 根据主键,单条删除
     */
    public Long deleteById(String id);

    /**
     * 根据主键列表，批量删除记录
     */
    public Long deleteByIds(List<String> list);
    
    /**
     * 根据主键,查询记录
     */
    public T queryById(String id);
    
    /**
     * 查询所有记录
     */
    public List<T> queryAll();
    
    /**
     * 查询所有记录(分页)
     */
    public Page<T> queryAllPage();
    
    /**
     * 根据条件查询记录
     */
    public List<T> queryByCondition(T condition);
    
    /**
     * 根据条件查询记录
     */
    public Page<T> queryByConditionPage(T condition);
    
    /**
     * 查询所有记录数量
     */
    public Long countAll();
    
    /**
     * 根据条件查询记录数量
     */
    public Long countByCondition(T condition);

}
