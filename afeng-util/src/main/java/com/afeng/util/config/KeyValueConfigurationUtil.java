package com.afeng.util.config;

/**
 * 
 * @Title       : KeyValueConfigurationUtil
 * @Description : 初始化项目中所有的配置文件，也是业务模块调用的入口util
 * @Date        : 2017年11月14日
 * @author      : afeng
 */
public class KeyValueConfigurationUtil {

    /**
     * 支付渠道
     */
    public static KeyValueConfiguration payChannelConfiguration;
    
    static {
        payChannelConfiguration = new KeyValueConfiguration("pay-channel.properties");
    }

}
