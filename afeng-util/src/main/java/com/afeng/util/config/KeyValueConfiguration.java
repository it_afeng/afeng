package com.afeng.util.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Component;

import com.afeng.util.log.DltagConstant;
import com.afeng.util.log.LogMessageUtil;

/**
 * 
 * @Title       : KeyValueConfiguration
 * @Description : 读取配置文件，根据key获取value
 * @Date        : 2017年11月14日
 * @author      : afeng
 */
@Component
public class KeyValueConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(KeyValueConfiguration.class);
    
    /*
     * 定义：解析对象后，产生的数据集合map
     */
    private Map<String, String> keyValueMap = new HashMap<>();

    public KeyValueConfiguration() {
    }

    /**
     * 构造方法，自动装配，每一个配置文件，只会初始化一次
     */
    @Autowired(required = false)
    public KeyValueConfiguration(String fileName) {
        
        PathMatchingResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver();
        Resource[] resources;
        /*
         * 记录配置文件初始化日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_LOAD_CONFIG_DLTAG);
        logMessageUtil.add("fileName", fileName);
        LOGGER.info(logMessageUtil.toString());
        try {
            resources = patternResolver.getResources(fileName);
            if (resources != null && resources.length > 0) {
                InputStreamReader isr = new InputStreamReader(resources[0].getInputStream(), "utf-8");
                BufferedReader reader = new BufferedReader(isr);
                String tempStr;
                while ((tempStr = reader.readLine()) != null) {
                    String[] configs = tempStr.split(",");
                    if (configs.length != 2) {
                        continue;
                    }
                    keyValueMap.put(configs[0], configs[1]);
                }
                IOUtils.closeQuietly(reader);
            }
        } catch (IOException e) {
            String errmsg = "读取参数列表失败，" + e.getMessage();
            logMessageUtil.setDltag(DltagConstant.COM_ERROR);
            logMessageUtil.add("errMsg", errmsg);
            LOGGER.error(logMessageUtil.toString());
        }
    }

    /**
     * 根据key，获取value值
     */
    public String getNameById(String id) {
        if (StringUtils.isBlank(id)) {
            return "";
        }
        String value = keyValueMap.get(id);
        if (StringUtils.isBlank(id)) {
            return "";
        }
        return value;
    }

    /**
     * 获取配置文件，解析成的map对象
     */
    public Map<String, String> getMap() {
        return keyValueMap;
    }

}
