package com.afeng.util.common;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @Title       : MathUtil
 * @Description : 数字工具类
 * @Date        : 2017年11月16日
 * @author      : afeng
 */
public class StringUtil {
    
    /**
     * 目标字符串，不够最小位数的，用替换字符替换
     * @param value 目标字符串
     * @param minDigit 最小位数
     * @param replaceStr 位数不够，替补的字符(默认‘0’)
     * @return
     */
    public static String resultFormat(String value, int minDigit,String replaceStr) {
        if (StringUtils.isBlank(replaceStr)) {
            replaceStr = "0";
        }
        if (StringUtils.isBlank(value)) {
            value = replaceStr;
        }
        if(minDigit <= 0) {
            return value;
        }
        while (value.length() < minDigit) {
            value = replaceStr + value;
        }
        return value;
    }

}
