package com.afeng.util.common;

/**
 * 
 * @Title       : DateUtil
 * @Description : 日期工具类
 * @Date        : 2017年11月16日
 * @author      : afeng
 */
public class DateUtil {
    
    /**
     * 获取当前时间的时间戳(秒)
     */
    public static long currentTimestampSecond() {
        return System.currentTimeMillis()/1000;
    }

}
