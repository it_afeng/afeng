package com.afeng.util.common;

/**
 * 
 * @Title       : MathUtil
 * @Description : 数字工具类
 * @Date        : 2017年11月16日
 * @author      : afeng
 */
public class MathUtil {
    
    /**
     * 获取一个大的随机数(0到2的32次方)
     */
    public static long randomLong() {
        long ret = (long) (Math.random() * Math.pow(2, 32));
        return ret;
    }

}
