package com.afeng.util.log;

import com.afeng.util.common.IpUtil;
import com.afeng.util.common.MathUtil;
import com.afeng.util.common.StringUtil;

/**
 * 
 * @Title       : TraceSpanIdGenerator
 * @Description : traceid/spanid生成工具类
 * @Date        : 2017年11月16日
 * @author      : afeng
 */
public class TraceSpanIdGeneratorUtil {

    /**
     * 生成traceid或者spanid工具方法
     */
    public static String generateTraceIdOrSpanId() {
        //当前时间戳（毫秒）
        long timeFactor = System.currentTimeMillis();
        // ip种子+时间种子
        long idpart1 = IpUtil.getLocalIp2Long() << 32 | (timeFactor & 0xffffffff);
        // 随机种子
        long idpart2 = MathUtil.randomLong() << 32 | (MathUtil.randomLong() & 0xffffff) ;
        return StringUtil.resultFormat(Long.toHexString(idpart1), 16,"0") + StringUtil.resultFormat(Long.toHexString(idpart2), 16,"0");
    }

}
