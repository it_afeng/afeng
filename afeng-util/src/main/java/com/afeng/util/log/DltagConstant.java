package com.afeng.util.log;

/**
 * 
 * @Title       : DltagConstant
 * @Description : 日志标签常量定义
 * @Date        : 2017年11月17日
 * @author      : afeng
 */
public interface DltagConstant {
    
    /**
     * default:undef
     */
    public static final String DEFAULT_DLTAG = "undef";
    
    /**
     * com_error
     */
    public static final String COM_ERROR = "com_error";
    
    /**
     * com_mysql_add
     */
    public static final String COM_MYSQL_ADD_DLTAG = "com_mysql_add";
    
    /**
     * com_mysql_delete
     */
    public static final String COM_MYSQL_DELETE_DLTAG = "com_mysql_delete";
    
    /**
     * com_mysql_update
     */
    public static final String COM_MYSQL_UPDATE_DLTAG = "com_mysql_update";

    /**
     * com_mysql_query
     */
    public static final String COM_MYSQL_QUERY_DLTAG = "com_mysql_query";
    
    /**
     * com_ehcache3_event
     */
    public static final String COM_EHCACHE3_EVENT_DLTAG = "com_ehcache3_event";
    
    /**
     * com_redis_set
     */
    public static final String COM_REDIS_SET_DLTAG = "com_redis_set";
    
    /**
     * com_redis_delete
     */
    public static final String COM_REDIS_DELETE_DLTAG = "com_redis_delete";
    
    /**
     * com_redis_get
     */
    public static final String COM_REDIS_GET_DLTAG = "com_redis_get";
    
    /**
     * com_memcached_set
     */
    public static final String COM_MEMCACHED_SET_DLTAG = "com_memcached_set";
    
    /**
     * com_memcached_delete
     */
    public static final String COM_MEMCACHED_DELETE_DLTAG = "com_memcached_delete";
    
    /**
     * com_memcached_get
     */
    public static final String COM_MEMCACHED_GET_DLTAG = "com_memcached_get";
    
    /**
     * com_mongo_save
     */
    public static final String COM_MONGO_SAVE_DLTAG = "com_mongo_save";
    
    /**
     * com_read_config
     */
    public static final String COM_READ_CONFIG_DLTAG = "com_read_config";
    
    /**
     * com_load_config
     */
    public static final String COM_LOAD_CONFIG_DLTAG = "com_load_config";
    
    /**
     * com_send_email
     */
    public static final String COM_SEND_EMAIL_DLTAG = "com_send_email";
    
    /**
     * com_scheduled
     */
    public static final String COM_SCHEDULED_DLTAG = "com_scheduled";
    
}
