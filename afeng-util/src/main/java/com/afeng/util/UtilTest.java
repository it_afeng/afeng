package com.afeng.util;

import com.afeng.util.log.LogMessageUtil;

public class UtilTest {

    public static void main(String[] args) {
        
        /**
         * 日志工具类测试
         */
        LogMessageUtil logUtil = new LogMessageUtil();
        LogMessageUtil.addToThreadLocal("traceid", "123d");
        LogMessageUtil.addToThreadLocal("spanid", "465623d");
        logUtil.setDltag("com_request_in");
        logUtil.add("user", "zhangsan");
        logUtil.add("age", "12");
        System.out.println(logUtil.toString());
        
        LogMessageUtil logUtil2 = new LogMessageUtil();
        logUtil2.setDltag("com_request_out");
        logUtil2.add("user", "lisi");
        System.out.println(logUtil2.toString());
    }

}
