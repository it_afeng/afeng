package com.afeng.util.result;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 
 * @Title       : JsonResult
 * @Description : 返回json对象定义
 * @Date        : 2017年11月20日
 * @author      : afeng
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonResult {
    
    /**
     * 状态码（默认，成功：200，失败：500）
     */
    private Integer code = ResultCodeConstant.SUCCESS_CODE;
    /**
     * 消息
     */
    private String msg;
    /**
     * 数据总条数
     */
    private Long total;
    /**
     * 数据集合
     */
    private Object data;
    
    /**
     * 其他数据集合
     */
    private Map<String, Object> dataMap;
    
    /**
     * 处理成功后需要首先调用的封装方法
     */
    public static JsonResult success() {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setCode(ResultCodeConstant.SUCCESS_CODE);
        return jsonResult;
    }

    /**
     * 处理失败后需要首先调用的封装方法
     */
    public static JsonResult fail(String msg) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setCode(ResultCodeConstant.FAIL_CODE);
        jsonResult.setMsg(msg);
        return jsonResult;
    }

    public String getMsg() {
        return msg;
    }

    public JsonResult setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public Long getTotal() {
        return total;
    }

    public JsonResult setTotal(Long total) {
        this.total = total;
        return this;
    }

    public Integer getCode() {
        return code;
    }

    public JsonResult setCode(Integer code) {
        this.code = code;
        return this;
    }

    public Object getData() {
        return data;
    }

    public JsonResult setData(Object data) {
        this.data = data;
        return this;
    }

    public Map<String, Object> getDataMap() {
        return dataMap;
    }

    public JsonResult setDataMap(Map<String, Object> dataMap) {
        this.dataMap = dataMap;
        return this;
    }
    
    public JsonResult put(String key, Object value) {
        if(dataMap == null) {
            dataMap = new HashMap<>();
        }
        dataMap.put(key, value);
        return this;
    }
    
}
