package com.afeng.util.result;

/**
 * 
 * @Title       : ResultCodeConstant
 * @Description : 结果状态码常量定义
 * @Date        : 2017年11月20日
 * @author      : afeng
 */
public interface ResultCodeConstant {
    
    /**
     * success default code
     */
    public static final Integer SUCCESS_CODE = 200;
    
    /**
     * fail default code
     */
    public static final Integer FAIL_CODE = 500;
    
}
