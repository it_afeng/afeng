package com.afeng.util.spring;

import org.springframework.context.ApplicationContext;
/**
 * 
 * @Title       : SpringContextUtil
 * @Description : Spring上下文工具类
 * @Date        : 2017年11月13日
 * @author      : afeng
 */
public class SpringContextUtil {

    /**
     * Spring上下文对象定义
     */
    private static ApplicationContext applicationContext;

    /**
     * 获取Spring上下文对象
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * 设置Spring上下文对象
     */
    public static void setApplicationContext(ApplicationContext applicationContext) {
        SpringContextUtil.applicationContext = applicationContext;
    }

    /**
     * 根据名称获取上下文中的bean实例对象
     */
    public static Object getBean(String name) {
        return applicationContext.getBean(name);
    }

    /**
     * 根据类型获取上下文中的bean实例对象
     */
    public static Object getBean(Class<?> requiredType) {
        return applicationContext.getBean(requiredType);
    }

}
