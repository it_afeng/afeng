# 总纲
	沉淀技术，实现互联网高效实用开发框架。

# 实用、易用、好用、干货多多
	1、模块独立，按需取用。
	2、完善的注解和示例，对工具类和模版类的封装会持续更新。
	3、干货文档，doc目录全是在梳理过程中，总结的实用doc。
	4、业界最流行的框架、db、中间件、大数据处理和分析、日志的分析抽取清洗、调优、监控、运维。
	5、负载、高可用。

# 整体规划一期
	1、spring-boot环境，rest服务，项目服务架构，代码架构。（完成）
	2、spring上下文设置，key-value形式配置文件解析封装。（完成）
	3、logback日志框架，日志规范定义，日志工具类模块封装。（完成）
	4、通过拦截器，请求参数封装全局threadlocal。（完成）
	5、Mysql：druid连接池、mybatis，多数据源配置，mysql公用方法范型封装、分页插件PageHelper引入并示例。（完成）
	6、以mysql的增删改查为例，实现请求trace日志全链路模型。(完成)
	7、spring rest返回前端json数据类型定义封装。(完成)
	8、引入缓存Ehcache3，记录缓存事件日志。（完成）
	9、引入缓存数据库redis、memcached、mongodb。（完成）
	10、异常模型，Controlleradvice+ExceptionHandler实现全局的异常处理。（完成）
	11、引入es。包括：es、kibana、X-Pack，安装、配置及使用示例。（完成）
	12、发送邮件mail，简单邮件、html邮件、附件邮件。（完成）
	13、scheduled定时器调度。（完成）
	14、线程池executor封装。（完成）
	10、mq:rabbitMQ封装。（完成）
	11、jms、websocket。
	12、安全。
	13、测量和监控。
	14、mobile，手机版本。
	15、社交：facebook、linkedin、twitter。
	16、ssh。

# 整体规划二期
	spring-clond

# 整体规划三期
	系统的各种参数调优，jvm、web容器。

# 整体规划四期
	实现一个权限系统

# 整体规划五期
	业务系统log 全链路trace

# 整体规划六期
	日志分析、抽取、清洗

# 整体规划七期
	kafka、flink、spark streaming、druid、es。
	flume、storm、hadoop、hive、hbase

# 整体规划八期
	监控预警系统，各类监控大盘等
	
# 整体规划九期
	自动化运维
	
# 参考文章	
	https://github.com/spring-projects/spring-boot/tree/master/spring-boot-samples
	http://blog.csdn.net/musuny/article/category/7043845
	http://www.ityouknow.com/share/2017/10/01/resource-sharing.html
	https://github.com/JeffLi1993/springboot-learning-example
