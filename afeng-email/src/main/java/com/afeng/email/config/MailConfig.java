package com.afeng.email.config;

import java.io.File;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

/**
 * 
 * @Description : 初始化email配置类
 * @Date : 2017年12月02日
 * @author : afeng
 */
@Component
@PropertySource("classpath:email.properties")
public class MailConfig {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JavaMailSender mailSender;

    /**
     * 发送者
     */
    @Value("${mail.fromMail.addr}")
    private String from;

    /**
     * 
     * @param to
     *            发送给谁
     * @param subject
     *            主题
     * @param content
     *            内容
     */
    public void sendSimpleMail(String to, String subject, String content) {

        /*
         * 测试普通邮件
         */
        /*
         * SimpleMailMessage message = new SimpleMailMessage();
         * message.setFrom(from); message.setTo(to);
         * message.setSubject(subject); message.setText(content);
         * 
         * try { mailSender.send(message); logger.info("简单邮件已经发送。"); } catch
         * (Exception e) { logger.error("发送简单邮件时发生异常！", e); }
         */

        /*
         * 测试html邮件
         */
        /*MimeMessage mimeMessage = null;
        try {
            mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setFrom(from);
            helper.setTo(to);
            helper.setSubject("标题：发送Html内容");

            StringBuffer sb = new StringBuffer();
            sb.append("<h1>大标题-h1</h1>")
                .append("<p style='color:#F00'>红色字</p>")
                .append("<p style='text-align:right'>右对齐</p>")
                .append("<p style='text-align:right'>" + content + "</p>");
            helper.setText(sb.toString(), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mailSender.send(mimeMessage);*/
        
        /*
         * 测试附件邮件
         */
        MimeMessage message = null;
        try {
            message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(to);
            helper.setSubject("主题：带附件的邮件");
            helper.setText(content);
            //注意项目路径问题
            FileSystemResource file = new FileSystemResource(new File("/Users/didi/Documents/介绍2.jpeg"));
            //加入邮件
            helper.addAttachment("图片222.jpg", file);
        } catch (Exception e){
            e.printStackTrace();
        }
        mailSender.send(message);

    }
}