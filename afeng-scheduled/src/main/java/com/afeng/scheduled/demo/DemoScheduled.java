package com.afeng.scheduled.demo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.afeng.util.log.DltagConstant;
import com.afeng.util.log.LogMessageUtil;

/**
 * 
 * @Description : 定时器demo
 * @Date : 2017年12月02日
 * @author : afeng
 */
@Component
public class DemoScheduled {

    private final Logger logger = LoggerFactory.getLogger(DemoScheduled.class);

    /**
     * 定时器示例方法
     */
    @Scheduled(cron = "0/2 * * * * *")  
    public void timerDemo(){  
        //获取当前时间  
        LocalDateTime localDateTime =LocalDateTime.now();  
        
        /*
         * 记录业务日志
         */
        LogMessageUtil logMessageUtil = new LogMessageUtil();
        logMessageUtil.setDltag(DltagConstant.COM_SCHEDULED_DLTAG);
        logMessageUtil.add("localDateTime", localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        logger.info(logMessageUtil.toString());
    } 
}