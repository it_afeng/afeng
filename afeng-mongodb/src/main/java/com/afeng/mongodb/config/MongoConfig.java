package com.afeng.mongodb.config;

import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.MongoClientURI;

/**
 * 
 * @Description : mongo配置类，指定自定义mongo配置文件
 * @Date        : 2017年11月26日
 * @author      : afeng
 */
@Configuration
@PropertySource("classpath:mongodb.properties")
public class MongoConfig {
    
    /**
     * mongo uri定义
     */
    @Value("${spring.data.mongodb.uri}")
    private String MONGO_URI;

    @Bean
    @Primary
    public MongoDbFactory dbFactory() throws UnknownHostException {
        return new SimpleMongoDbFactory(new MongoClientURI(MONGO_URI));
    }

    @Bean
    @Primary
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(this.dbFactory());
    }

}