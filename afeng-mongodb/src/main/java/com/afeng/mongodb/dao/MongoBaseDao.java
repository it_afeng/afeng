package com.afeng.mongodb.dao;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

/**
 * 
 * @Title       : MongoBaseDao
 * @Description : demo模块mongo数据查询基础封装服务
 * @Date        : 2017年11月26日
 * @author      : afeng
 */
@Component
public class MongoBaseDao<T>{

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 新增
     */
    public void save(T t) {
        mongoTemplate.save(t);
    }
    
    /**
     * 删除（根据主键id删除对象）
     */
    public void deleteById(String id,Class<T> c) {
        Query query=new Query(Criteria.where("id").is(id));
        mongoTemplate.remove(query,c);
    }
    
    /**
     * 根据指定的主键及值，删除对象
     */
    public void deleteById(String idColumnName, String idValue,Class<T> c) {
        if(StringUtils.isBlank(idColumnName)) {
            idColumnName = "id";
        }
        Query query=new Query(Criteria.where(idColumnName).is(idValue));
        mongoTemplate.remove(query,c);
    }
    
    /**
     * 根据指定的主键及值，查询对象
     */
    public T findById(String idColumnName, String idValue, Class<T> c) {
        if(StringUtils.isBlank(idColumnName)) {
            idColumnName = "id";
        }
        Query query=new Query(Criteria.where(idColumnName).is(idValue));
        T t =  (T)mongoTemplate.findOne(query , c);
        return t;
    }
    
    /**
     * 根据id查询对象
     */
    public T findById(String id, Class<T> c) {
        Query query=new Query(Criteria.where("id").is(id));
        T t =  (T)mongoTemplate.findOne(query , c);
        return t;
    }

    /**
     * 根据用户名查询对象
     * @param userName
     * @return
     */
//    public T findUserByUserName(String userName, Class cl) {
//        Query query=new Query(Criteria.where("userName").is(userName));
//        T user =  (T)mongoTemplate.findOne(query , cl);
//        return user;
//    }

    /**
     * 更新对象
     * @param user
     */
//    public void updateUser(T user) {
//        Query query=new Query(Criteria.where("id").is(user.getId()));
//        Update update= new Update().set("userName", user.getUserName()).set("passWord", user.getPassWord());
//        //更新查询返回结果集的第一条
//        mongoTemplate.updateFirst(query,update,UserEntity.class);
//        //更新查询返回结果集的所有
//        // mongoTemplate.updateMulti(query,update,UserEntity.class);
//    }

    
}