package com.afeng.redis.config;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

/**
 * 
 * @Title : RedisConfigUtil
 * @Description : redis工具类
 * @Date : 2017年11月23日
 * @author : afeng
 */
@Component
public class RedisConfigUtil {

    /**
     * 常用的字符串操作模版
     */
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 根据key设置value
     */
    public void set(String key, String value) {
        if (StringUtils.isEmpty(key)) {
            return;
        }
        stringRedisTemplate.opsForValue().set(key, value);
    }

    /**
     * 根据key设置value,并设置过期时间（秒）
     */
    public void set(String key, String value, long timeout) {
        if (StringUtils.isEmpty(key)) {
            return;
        }
        stringRedisTemplate.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
    }

    /**
     * 根据key获取value
     */
    public String get(String key) {
        if (StringUtils.isEmpty(key)) {
            return null;
        }
        String value = stringRedisTemplate.opsForValue().get(key);
        return value;
    }
    
    /**
     * 删除key
     */
    public void delete(String key) {
        if (StringUtils.isEmpty(key)) {
            return ;
        }
        stringRedisTemplate.delete(key);;
    }

}
